import java.io.FileInputStream
import java.util.Properties

plugins {
    jacoco
    signing
    `java-library`
    `java-library-distribution`
    `maven-publish`
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.git.version)
    alias(libs.plugins.nexus.publish)
}

// we handle cases without .git directory
val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
val details = versionDetails()
val baseVersion = details.lastTag.substring(1)
version = when {
    details.isCleanTag -> baseVersion
    else -> baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
}

group = "tokyo.northside"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.dictzip)
    implementation(libs.trie4j)
    testImplementation(libs.junit.jupiter)
}

spotbugs {
    // excludeFilter.set(project.file("config/spotbugs/exclude.xml"))
    tasks.spotbugsMain {
        reports.create("html") {
            required.set(true)
        }
    }
    tasks.spotbugsTest {
        reports.create("html") {
            required.set(true)
        }
    }
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

jacoco {
    toolVersion="0.8.6"
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
    reports {
        xml.required.set(false)
        html.required.set(true)
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of("11"))
    }
    withSourcesJar()
    withJavadocJar()
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

// we handle cases without .git directory
val versionProperties : File = project.file("src/main/resources/io/github/eb4j/stardict/version.properties")
val dotgit: File = project.file(".git")

if (dotgit.exists()) {
    apply(plugin = libs.plugins.git.version.get().pluginId)

    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
                pom {
                    name.set("stardict4j")
                    description.set("Stardict access library for java")
                    url.set("https://codeberg.org/miurahr/stardict4j")
                    licenses {
                        license {
                            name.set("The GNU General Public License, Version 3")
                            url.set("https://www.gnu.org/licenses/licenses/gpl-3.html")
                            distribution.set("repo")
                        }
                    }
                    developers {
                        developer {
                            id.set("miurahr")
                            name.set("Hiroshi Miura")
                            email.set("miurahr@linux.com")
                        }
                    }
                    scm {
                        connection.set("scm:git:git://codeberg.org/miurahr/stardict4j.git")
                        developerConnection.set("scm:git:git://codeberg.org/miurahr/stardict4j.git")
                        url.set("https://codeberg.org/miurahr/stardict4j")
                    }
                }
            }
        }
    }

    val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find { project.hasProperty(it) }
    tasks.withType<Sign> {
        onlyIf { details.isCleanTag && (signKey != null) }
    }

    signing {
        when (signKey) {
            "signingKey" -> {
                val signingKey: String? by project
                val signingPassword: String? by project
                useInMemoryPgpKeys(signingKey, signingPassword)
            }
            "signing.keyId" -> { /* do nothing */
            }
            "signing.gnupg.keyName" -> {
                useGpgCmd()
            }
        }
        sign(publishing.publications["mavenJava"])
    }

    nexusPublishing.repositories {
        sonatype()
    }
} else if (versionProperties.exists()) {
    version = Properties().apply { load(FileInputStream(versionProperties)) }.getProperty("version")
}

tasks.register("writeVersionFile") {
    val folder = project.file("src/main/resources/tokyo/northside/stardict")
    if (!folder.exists()) {
        folder.mkdirs()
    }
    versionProperties.delete()
    versionProperties.appendText("version=" + project.version)
}

tasks.getByName("jar") {
    dependsOn("writeVersionFile")
}

tasks.jacocoTestCoverageVerification {
    dependsOn(tasks.test)
    violationRules {
        rule {
            element = "CLASS"
            includes = listOf("**.stardict.**")
            limit {
                minimum = "0.85".toBigDecimal()
            }
        }
    }
}

spotless {
    format("misc") {
        target(listOf("*.gradle", ".gitignore", "*.rst"))
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
    java {
        target(listOf("src/*/java/**/*.java"))
        removeUnusedImports()
        importOrder()
        palantirJavaFormat()
    }
}
