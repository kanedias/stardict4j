module tokyo.northside.stardict4j {
    requires java.base;
    requires tokyo.northside.dictzip.lib;
    requires tokyo.northside.trie4j;

    exports tokyo.northside.stardict;
}
