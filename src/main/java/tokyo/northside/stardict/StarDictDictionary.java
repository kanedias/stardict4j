/*
 * Stardict4j - access library for stardict format.
 * Copyright (C) 2022 Hiroshi Miura.
 * Copyright (C) 2009 Alex Buloichik
 *               2015-2016 Hiroshi Miura, Aaron Madlon-Kay
 *               2020 Suguru Oho, Aaron Madlon-Kay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package tokyo.northside.stardict;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import tokyo.northside.stardict.cache.SimpleLRUCache;

/**
 * Abstract base class to represent StarDict dictionary data.
 */
public abstract class StarDictDictionary implements AutoCloseable {

    private final Map<IndexEntry, String> cache;
    private final boolean enableCache;
    /** dictionary index data. */
    protected final DictionaryData<IndexEntry> data;
    /** dictionary metadata. */
    protected final StarDictInfo info;

    /**
     * Backward compatible constructor.
     * @param data collection of <code>IndexEntry</code>s loaded from file
     * @param maxsize max size of cache.
     * @param duration duration to keep in cache.
     * @param info metadata info.
     */
    @SuppressWarnings("unused")
    @Deprecated()
    StarDictDictionary(
            final DictionaryData<IndexEntry> data,
            final StarDictInfo info,
            final int maxsize,
            final Duration duration) {
        this(data, info, 16, maxsize);
    }

    /**
     * Default constructor.
     * @param data collection of <code>IndexEntry</code>s loaded from file
     * @param info metadata info.
     * @param initialCacheSize initial capacity of cache
     * @param maximumCacheSize max size of cache.
     */
    StarDictDictionary(
            final DictionaryData<IndexEntry> data,
            final StarDictInfo info,
            final int initialCacheSize,
            final int maximumCacheSize) {
        this.data = data;
        this.info = info;
        enableCache = maximumCacheSize > 0;
        if (enableCache) {
            cache = Collections.synchronizedMap(new SimpleLRUCache<>(initialCacheSize, maximumCacheSize));
        } else {
            cache = null;
        }
    }

    /**
     * Builder utility method for StarDictDictionary.
     * @param ifoFile .ifo file.
     * @return StarDictDictionary object.
     * @throws Exception when i/o error or parse error occurred.
     */
    public static StarDictDictionary loadDictionary(final File ifoFile) throws Exception {
        String f = ifoFile.getPath();
        if (f.endsWith(".ifo")) {
            f = f.substring(0, f.length() - ".ifo".length());
        }
        Path dictPath = Paths.get(f + ".dict.dz");
        if (dictPath.toString().endsWith(".dz")) {
            return StarDictLoader.load(ifoFile, 200);
        } else {
            return StarDictLoader.load(ifoFile, 100);
        }
    }

    /**
     * Builder utility method for StarDictDictionary.
     * @param ifoFile .ifo file.
     * @param cacheSize cache size of article.
     * @param duration cache expiry time.
     * @return StarDictDictionary object.
     * @throws Exception when i/o error or parse error occurred.
     */
    @SuppressWarnings("unused")
    public static StarDictDictionary loadDictionary(final File ifoFile, final int cacheSize, final Duration duration)
            throws Exception {
        return StarDictLoader.load(ifoFile, cacheSize);
    }

    /**
     * Builder utility method for StarDictDictionary.
     * @param ifoFile .ifo file.
     * @param cacheSize cache size of article.
     * @return StarDictDictionary object.
     * @throws Exception when i/o error or parse error occurred.
     */
    public static StarDictDictionary loadDictionary(final File ifoFile, final int cacheSize) throws Exception {
        return StarDictLoader.load(ifoFile, cacheSize);
    }

    /**
     * get human-readable name.
     * @return name
     */
    public String getDictionaryName() {
        return info.getBookName();
    }

    /**
     * Get dictionary version.
     * @return version whether "2.4.2" or "3.0.0"
     */
    public String getDictionaryVersion() {
        return info.getVersion();
    }

    /**
     * return dictionary information class.
     * @return StarDictInfo object.
     */
    public StarDictInfo getInformation() {
        return info;
    }

    /**
     * Search word exactly.
     * @param word to search.
     * @return list of result entries.
     */
    public List<Entry> readArticles(final String word) {
        List<Entry> list = new ArrayList<>();
        for (Map.Entry<String, IndexEntry> e : data.lookUp(word)) {
            Entry entry = new Entry(e.getValue().getWord(), getType(e.getValue()), getArticle(e.getValue()));
            list.add(entry);
        }
        return list;
    }

    /**
     * Read article with word prediction.
     * <p>
     *     you will get `superman` when search `super`
     * </p>
     * @param word search word
     * @return list of result entries.
     */
    public List<Entry> readArticlesPredictive(final String word) {
        List<Entry> list = new ArrayList<>();
        for (Map.Entry<String, IndexEntry> e : data.lookUpPredictive(word)) {
            Entry entry = new Entry(e.getValue().getWord(), getType(e.getValue()), getArticle(e.getValue()));
            list.add(entry);
        }
        return list;
    }

    private synchronized EntryType getType(final IndexEntry starDictEntry) {
        return starDictEntry.getType();
    }

    private synchronized String getArticle(final IndexEntry indexEntry) {
        if (enableCache) {
            String article = cache.get(indexEntry);
            if (article == null) {
                article = readArticle(indexEntry.getStart(), indexEntry.getLen());
                cache.put(indexEntry, article);
            }
            return article;
        } else {
            return readArticle(indexEntry.getStart(), indexEntry.getLen());
        }
    }

    /**
     * Read data from the underlying file.
     *
     * @param start Start offset in data file
     * @param len   Length of article data
     * @return Raw article text
     */
    protected abstract String readArticle(long start, int len);

    public void close() throws IOException {
        cache.clear();
    }

    /**
     * Entry types.
     */
    public enum EntryType {
        /** Word's pure text meaning. */
        MEAN('m'),
        /** English phonetic string.  */
        PHONETIC('t'),
        /** A string which is marked up with the Pango text markup language. */
        PANGO('g'),
        /** A string which is marked up with the XDXF language. */
        XDXF('x'),
        /** Chinese YinBao or Japanese KANA. */
        YINBAO('y'),
        /** KingSoft PowerWord's XML data. */
        KINGSOFT('k'),
        /** MediaWiki markup language. */
        MEDIAWIKI('w'),
        /** html codes. */
        HTML('h'),
        /** WordNet data. */
        WORDNET('n'),
        /** Resource file list. */
        RESOURCE('r'),
        /** WAVE file. */
        WAV('W'),
        /** Picture image. */
        PICTURE('P'),
        /** Reserved for experimental extension. */
        EXPERIMENTAL('X'),
        /** No same type. */
        NOSAMETYPE('$');

        private final char typeValue;

        EntryType(final char type) {
            typeValue = type;
        }

        /**
         * return type.
         * @return type value.
         */
        public char getTypeValue() {
            return typeValue;
        }

        /**
         *  return type from raw value.
         * @param c character indicate type.
         * @return type.
         */
        public static EntryType getTypeByValue(final char c) {
            for (EntryType t : EntryType.values()) {
                if (t.getTypeValue() == c) {
                    return t;
                }
            }
            return null;
        }
    }

    /**
     * Dictionary article data class.
     */
    public static class Entry {

        private final String word;
        private final EntryType type;
        private final String article;

        /**
         * constructor of data class.
         * @param word search word.
         * @param type entry type.
         * @param article article text.
         */
        public Entry(final String word, final EntryType type, final String article) {
            this.word = word;
            this.type = type;
            this.article = article;
        }

        /**
         * return entry word.
         * @return entry word.
         */
        public String getWord() {
            return word;
        }

        /**
         * Return entry type.
         * @return type enum value.
         */
        public EntryType getType() {
            return type;
        }

        /**
         * return article.
         * @return article.
         */
        public String getArticle() {
            return article;
        }
    }
}
