# Stardict4j - access library for stardict dictionary

StarDict file is one of popular dictionary data formats.
Stardict4j is an access library of StarDict dictionary file for Java.

stardict4j supports `.ifo`, `.dict` or `.dict.dz`, `.syn`,
and `.idx` or `.idx.gz` files.
Stardict4j loads an index data and parses its index into memory.

## Development status

A status of library development is considered as `Beta`.

## Install

Versions from v0.4.0 and later use group id `tokyo.northside` and package path is `io.github.eb4j.stardict4j`
by a historical reason.
Now the home of project is on codeberg.org that is not on GitHub, so we use new group id.

### Apache Maven

<details>

```xml
<dependency>
  <groupId>tokyo.northside</groupId>
  <artifactId>stardict4j</artifactId>
  <version>0.5.1</version>
</dependency>
```

</details>

### Gradle Groovy DSL

<details>

```groovy
implementation 'tokyo.northside:stardict4j:0.5.1'
```

</details>

### Gradle kotlin DSL

<details>

```
implementation("tokyo.northside:stardict4j:0.5.1")
```

</details>

### Scala SBT

<details>

```
libraryDependencies += "tokyo.northside" % "stardict4j" % "0.5.1"
```

</details>

## Use

Stardict4j provides a dictionary loader. You should call `StarDictDictionary#loadDictionary` method
to load `.idx` and `.syn` file. The method return `StarDictDictionary` object that has

methods `lookup` and `lookupPredictive`. The former method search word, and the latter is predictive,
run prefix search for word. These method returns `List<DictionaryEntry>`.

`StarDictDictionary#loadDictionary` method takes a File object of `.ifo` file or basename of dictionary files.
It also optionally takes two arguments for cache control, maxSize and duration.
The library will cache read articles in maxSize entries in duration expiry.

Each `DictionaryEntry` entry has `type` of entry such as `MEAN`, `HTML` or others, that can be retrieved with
`getType()` method.

### Example

Here is a simple example of how to use it.

```java
import tokyo.northside.stardict.StarDictDictionary;

public class Example {
    public void example() {
        String word = "testudo";
        StarDictDictionary dict = StarDictDictionary.loadDictionary(
                new File("dictionayr.ifo"), 500, Duration.ofMinutes(10));
        for (StarDictDictionary.Entry en : dict.readArticles(word)) {
            switch (en.getType()) {
                case MEAN -> System.out.println(String.format("%s has meanings of %s\n", en.getWord(), en.getArticle()));
                case PHONETIC -> System.out.println(String.format("%s pronounce is %s\n", en.getWord(), en.getArticle()));
                default -> {
                }
            }
        }
    }
}
```
